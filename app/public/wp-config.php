<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vBNM/9f1bUuxaVLx6ztz5dcHZcMlDGLSRZ8rTTUaBnmNJoiCgyCjimW5b0m43omZsug474CtkQmXNarCqd9fkQ==');
define('SECURE_AUTH_KEY',  'hWY01Mxpw0ZWEWHNwTUHWvMuLfFjYoDCujm8VbbQ4IGsoqJX7uzzWnH1oOWJPKZ26POCmiLlMb0R7lHfMMs1+Q==');
define('LOGGED_IN_KEY',    'CqylcpmhUkbDrDGuENmSi7fGQM/UkaWEL5p5Khmba3Hg+LJxlB7/C/q28MIhK1Uy1WzfApJefUpxxaOAmTOmnQ==');
define('NONCE_KEY',        'Mc7yYNsdQIo6DBwjbDw5xdOGJPNJySPietniqpQG/ICqWCWw6C3j+2mChSD+LgeTlNDI772LwbUKYWOdiraHIw==');
define('AUTH_SALT',        'WMpZi4fRiLxs+S1AfTPGwLDSeIT9mYx0nNBejCClZAC4sQiNW8IMsqOGJomwSaUOkDiXqRDzrG+oJWHUBWsrDQ==');
define('SECURE_AUTH_SALT', 'NQZJNmj54OwCO2KXFyrTpgqNFnnMf/97SoxCyzhmhgpjCQtMbhCPJk0h+6NDRxo+NNzEons0BnATLIK1g1mg8A==');
define('LOGGED_IN_SALT',   '+I39y1LGWg6PiGqf5AVHd9+RIZAKH2yAQPAXyPCcQOZSXw8iBBowAniOrzKxOmTKSrGzP9cGXXqsZDkWYik71A==');
define('NONCE_SALT',       'EcnSeRaLxTemUqP1Et2FEdR+KcyxDnG8+6MqbZgFDQS+3Mg1MY2plt92n7ndDhLUt32UXWhm5U2GoCT5WmbI5w==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
